/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appoo2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 *
 * @author werT Jr
 */
public class Appoo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //condition 1
        Queue<Book> books = new LinkedList<>();

        Book book = new Book("Stephen King", "Misery", 250, 50.5f);

        books.add(book);

        book = new Book("J. R. R. Tolkien", "The Silmarillion", 400, 100.57f);
        books.add(book);

        book = new Book("J. R. R. Tolkien", "The hobbit", 251, 51.5f);
        books.add(book);

        book = new Book("Isaac Asimov", "Foundation I", 450, 20.5f);
        books.add(book);

        book = new Book("Isaac Asimov", "Foundation II", 455, 23.5f);
        books.add(book);

        book = new Book("Isaac Asimov", "Foundation III", 460, 25.5f);
        books.add(book);

        //condition 2
        for (Book item : books) {
            System.out.println(item);
            System.out.println();
        }

        //condition 3
        books.remove();
        books.remove();
        books.remove();
        books.remove();

        book = new Book("Camil Petrescu", "Patul lui Procust", 468, 28.5f);
        books.add(book);

        //condition 4
        Iterator it = books.iterator();

        System.out.println("\t\tIterator: \n");
        while (it.hasNext()) {
            System.out.println(it.next());
            System.out.println();
        }

        //condition 5
        Queue<Book> secondQueue = new LinkedList<>();

        book = new Book("Liviu Rebreanu", "Pădurea spânzuraţilor", 460, 25.5f);
        secondQueue.add(book);
        book = new Book("Shakespeare", "Hamlet", 500, 29.5f);
        secondQueue.add(book);
        book = new Book("F. Scott Fitzgerald", "Marele Gatsby", 300, 25.5f);
        secondQueue.add(book);

        //condition 6
        Scanner in = new Scanner(System.in);

        System.out.println("\t\tNumarul de elemente: ");
        int number = in.nextInt();

        for (int i = 0; i < number; i++) {
            books.remove();
        }
        for (Book item : secondQueue) {
            books.add(item);
        }

        //condition 7
        System.out.println("\n\t\tContainer1\n");
        for (Book item : books) {
            System.out.println(item);
            System.out.println();
        }

        System.out.println("\n\t\tContainer2\n");
        for (Book item : secondQueue) {
            System.out.println(item);
            System.out.println();
        }
    }
}
