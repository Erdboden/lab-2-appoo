/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appoo2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author werT Jr
 */
public class Appoo2_b {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        
        //condition 1
        Stack<Book> stack1 = new Stack<>();
        
        Book book = new Book("Stephen King", "Misery", 250, 50.5f);
        stack1.push(book);
        
 book = new Book("J. R. R. Tolkien", "The Silmarillion", 400, 100.57f);
        stack1.push(book);
        
       book = new Book("J. R. R. Tolkien", "The hobbit", 251, 51.5f);
        stack1.push(book);
        
        book = new Book("Isaac Asimov", "Foundation I", 450, 20.5f);
        stack1.push(book);
        
        book = new Book("Isaac Asimov", "Foundation II", 455, 23.5f);
        stack1.push(book);
        
        //condition 2
        List<Book> list = new ArrayList<>();
        while (!stack1.empty()){
            list.add(stack1.pop());
        }
        Collections.sort(list);
        
        for(int i =  list.size() -1; i >= 0; i--){
            stack1.push(list.get(i));
        }
        
        //condition 3
        System.out.println("\n\t\tContainer sortat descendent!");
        
        Iterator it = stack1.iterator();
        
        while(it.hasNext()){
            System.out.println(it.next() + "\n");
        }
        
        //condition 4
        it = stack1.iterator();
        List<Book> list2 = new ArrayList<>();
        System.out.println("\n\t\tObiecte gasite:\n");
        
        while(it.hasNext()){
            Book obj = (Book)it.next();
            if(obj.getTitle().equals("Misery")){
                System.out.println("\n\n"+obj);
                list2.add(obj);
            }
        }
        
        //condition 5 and 6 and 7 and 8
        Collections.sort(list2, Collections.reverseOrder());
        
        Stack<Book> stack2 = new Stack<>();
        
        for(int i = 0; i < list2.size(); i++){
            stack2.push(list2.get(i));
        }
        
        while (!stack1.empty()){
            stack1.pop();
        }
        
        for(int i = 0; i < list.size(); i++){
            stack1.push(list.get(i));
        }
        
        System.out.println("\n\t\tContainer 1 sortat ascendent!");
        
        it = stack1.iterator();
        
        while(it.hasNext()){
            System.out.println(it.next() + "\n");
        }
        
        System.out.println("\n\t\tContainer 2 sortat ascendent!");
        
        it = stack2.iterator();
        
        while(it.hasNext()){
            System.out.println(it.next() + "\n");
        }
        
        //condition 9
        Stack<Book> stack3 = new Stack<>();
        
        while(!stack1.empty()){
            stack3.push(stack1.pop());
        }
        while(!stack2.empty()){
            stack3.push(stack2.pop());
        }
        
        //condition 10
        it = stack3.iterator();
        
        System.out.println("\n\t\tContainer 3!");
        
        while(it.hasNext()){
            System.out.println("\n" + it.next());
        }
        
        //condition 11 and 12
        it = stack3.iterator();
        int count = 0;
                
        while(it.hasNext()){
            Book obj = (Book)it.next();
            if(obj.getTitle().equals("Misery")){
                count++;
            }
        }
        
        System.out.println("\nElemente gasite in container 3: " + count);
        
    }
    
}
