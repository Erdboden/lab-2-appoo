/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appoo2;



/**
 *
 * @author werT Jr
 */
public class Book implements Comparable<Book>{
    private String author;
    private String title;
    private int page;
    private float price;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Laptop{" + "author=" + author + ", title=" + title + ", page=" + page + ", price=" + price + '}';
    }

    public Book(String author, String title, int page, float price) {
        this.author = author;
        this.title = title;
        this.page = page;
        this.price = price;
    }

    @Override
    public int compareTo(Book o) {
  if (this.getPage()> o.getPage()) return 1; 
        else if (this.getPage() < o.getPage()) return -1; 
        else return 0; //if it's neither smaller nor larger, it must be equal
    }



}
